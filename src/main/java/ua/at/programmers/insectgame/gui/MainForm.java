package ua.at.programmers.insectgame.gui;

import java.awt.Font;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import java.util.Random;
import java.util.*;
import java.util.ArrayList;

import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.opengl.*;
import org.lwjgl.*;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import ua.at.programmers.insectgame.player.RaseBox;

public class MainForm{
    private int timePause = 1;
    private TrueTypeFont font1;
    private int sizeBox = 5;
    private int quantityBox = 50;
    private int numberPlayers = 4;
    private boolean isMenu = true;
    private boolean glMenuEnabled = false;
    public List<RaseBox> raseBox = null;

    public MainForm() throws InterruptedException {
    try {
        Display.setDisplayMode(new DisplayMode(640, 480));
        Display.setTitle("InsectGame");
        Display.create();
        Display.setVSyncEnabled(true);
        } catch (LWJGLException e) {
        e.printStackTrace();
    }

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, 640, 480, 0, 1, -1);
    glMatrixMode(GL_MODELVIEW);

    Font awtFont = new Font("Times New Roman", Font.BOLD, 24);
    font1 = new TrueTypeFont(awtFont, false);

    while(!Display.isCloseRequested()) {
        glClear(GL_COLOR_BUFFER_BIT);
        if (!glMenuEnabled && isMenu) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glMenuEnabled = true;
        }
        if (glMenuEnabled && !isMenu) {
        glDisable(GL_BLEND);
        glDisable(GL_ALPHA_TEST);
        glMenuEnabled = false;
        }
        if (isMenu && Keyboard.isKeyDown(Keyboard.KEY_RETURN))
        isMenu = false;
        if (!isMenu && Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
        isMenu = true;
        /*if (isMenu && Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
        Display.destroy();
        System.exit(0);
        }*/
        if (isMenu) {
        font1.drawString(640 / 2 - 125, 480 / 2 - 50, "Start Game <ENTER>", Color.yellow);
        }
        else {
            if (raseBox == null) {
                raseBox = new LinkedList<>();
                for (int i = 1; i < (numberPlayers + 1); i++) {
                    if (i % 4 == 0)
                        raseBox.add( new RaseBox(1, 1, quantityBox, sizeBox));
                    else if (i % 3 == 0)
                        raseBox.add( new RaseBox(640 - sizeBox, 1, quantityBox, sizeBox));
                    else if (i % 2 == 0)
                        raseBox.add( new RaseBox(640 - sizeBox, 480 - sizeBox, quantityBox, sizeBox));
                    else
                        raseBox.add( new RaseBox(1, 480 - sizeBox, quantityBox, sizeBox));
                }
            }
            else
                for (RaseBox rb : raseBox) {
                    timer(timePause);
                    rb.draw(this);
                }
        }
        Display.update();
        Display.sync(60);
    }
    Display.destroy();
    }

    public void timer(int timePause) throws InterruptedException {
        Thread.sleep(timePause);
    }

    public static void main(String args[]) throws InterruptedException  {
    new MainForm();
    }
}
