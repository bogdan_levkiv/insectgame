package ua.at.programmers.insectgame.player;

import java.util.Random;
import java.util.*;

import ua.at.programmers.insectgame.unit.Box;
import ua.at.programmers.insectgame.gui.MainForm;

public class RaseBox {
    private float colorRed, colorBlue, colorGreen;
    private int sizeBox;
    public List<Box> shapes = null;

    public RaseBox(int x, int y, int quantityBox, int sizeBox) {
        Random randomGenerator = new Random();
        colorRed = randomGenerator.nextFloat();
        colorBlue = randomGenerator.nextFloat();
        colorGreen = randomGenerator.nextFloat();
        int newX, newY;
        //shapes = new ArrayList<Box>(quantityBox);
        shapes = new LinkedList<>();
        for (int i = 1; i < (quantityBox + 1); i++) {
            if (i % 2 == 1) {
                newX = ((x > (640 / 2)) ? (x - i * sizeBox - 1) : (x + i * sizeBox + 1));
                newY = y;
            }
            else {
                newX = ((x > (640 / 2)) ? (x - i * sizeBox - 1) : (x + i * sizeBox + 1));
                newY = ((y > (480 / 2)) ? (y - sizeBox - 1) : (y + sizeBox + 1));
                }
            shapes.add(new Box(sizeBox, newX, newY, colorRed, colorBlue, colorGreen));
        }
    }
    public void draw(MainForm mf) {
        Iterator<Box> iterator = shapes.iterator(); 
        while (iterator.hasNext()) {
            iterator.next().draw(mf);
        }
    }
}
