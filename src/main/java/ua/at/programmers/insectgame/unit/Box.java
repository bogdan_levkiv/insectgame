package ua.at.programmers.insectgame.unit;

import static org.lwjgl.opengl.GL11.*;
import java.util.Random;

import ua.at.programmers.insectgame.gui.MainForm;
import ua.at.programmers.insectgame.player.RaseBox;

public class Box {
    static private int nId = 1;
    public int id;
    private int sizeBox;
    public int x, y;
    private int vecx, vecy;
    private float colorRed, colorBlue, colorGreen;
    
    public Box (int sizeBox, int x, int y, float colorRed, float colorBlue, float colorGreen) {
        this.sizeBox = sizeBox;
        this.x = x;
        this.y = y;
        vecx = 1;
        vecy = 1;
        this.colorRed = colorRed;
        this.colorBlue = colorBlue;
        this.colorGreen = colorGreen;
        this.id = nId;
        nId++;
    }
    
    boolean conflict (int x, int y) {
    if (((x >= this.x) && (x <= this.x + sizeBox) && (y >= this.y) && (y <= this.y + sizeBox))
        || ((x <= 0) || (x >= 640) || (y <= 0) || (y >= 480)))
        return true;
    return false;
    }
    
    public void draw (MainForm mf) {
        glColor3f(colorRed, colorGreen, colorBlue);
        
        for (RaseBox rb : mf.raseBox) {
            for (Box box : rb.shapes) {
                if ((this != box)) {
                    if (vecx > 0) {
                        if ((!box.conflict(this.x + 1 * vecx + sizeBox, this.y)) &&
                            (!box.conflict(this.x + 1 * vecx + sizeBox, this.y + sizeBox))) {
                            //this.x = this.x + 1 * vecx;
                        }
                        else {
                            vecx = vecx * -1;
                            //this.x = this.x + 1 * vecx;
                        }
                    }
                    else if (vecx < 0) {
                        if ((!box.conflict(this.x + 1 * vecx, this.y)) &&
                            (!box.conflict(this.x + 1 * vecx, this.y + sizeBox))) {
                            //this.x = this.x + 1 * vecx;
                        }
                        else {
                            vecx = vecx * -1;
                            //this.x = this.x + 1 * vecx;
                        }
                    }
                    if (vecy > 0) {
                        if ((!box.conflict(this.x, this.y + 1 * vecy + sizeBox)) &&
                            (!box.conflict(this.x + sizeBox, this.y + 1 * vecy + sizeBox))) {
                            //this.y = this.y + 1 * vecy;
                        }
                        else {
                            vecy = vecy * -1;
                            //this.y = this.y + 1 * vecy;
                        }
                    }
                    else if (vecy < 0) {
                        if ((!box.conflict(this.x, this.y + 1 * vecy)) &&
                            (!box.conflict(this.x + sizeBox, this.y + 1 * vecy))) {
                            //this.y = this.y + 1 * vecy;
                        }
                        else {
                            vecy = vecy * -1;
                            //this.y = this.y + 1 * vecy;
                        }
                    }
                }
            }
        }
        this.x = this.x + 1 * vecx;
        this.y = this.y + 1 * vecy;
        
        glBegin(GL_QUADS);
        glVertex2f(x, y);
        glVertex2f(x + sizeBox, y);
        glVertex2f(x + sizeBox, y + sizeBox);
        glVertex2f(x, y + sizeBox);
        glEnd();
        //System.out.println(id + " - " + x + " : " + y);
    }
}
